#iarg=1
#iarg=14
iarg=22
#mkdir output_exp
#declare -a StringArray=("10" "20" "50" "100" "200" "300" "400" "500" "600" "700" "800" "900" "1000")
#declare -a StringArray=("1500" "2000" "2500" "3000" "3500" "4000" "4500" "5000")
declare -a StringArray=("10000" "15000" "20000" "50000" "75000" "100000")
for bw in "${StringArray[@]}"; do
  (../bin/allpix -c testdensitycalc.conf -o DepositionLaser.number_of_photons=$bw > "output_density_calc/EXPOUT_"$iarg) >& "output_density_calc/EXPERR_"$iarg
  mv output/modulesTestDensityCalc.root "output_density_calc/modulesExp_"$iarg".root"
  
  iarg=`expr $iarg + 1`
done
