#!/bin/sh

# Usage:
# DoIRloop.sh NerOFRepetitions

#narg=$#

#iarg=1
#mkdir output_loops
#while (test "$iarg" -le "$narg")
#do

#  (../bin/allpix -c testdewit.conf > "STDOUT_"$iarg) >& "STDERR_"$iarg
#  mv output/modulesDeWit.root output_loops/modulesDeWit_"$iarg".root

#  iarg=`expr $iarg + 1`
#done

iarg=1
#mkdir output_exp
declare -a StringArray=("10.0um" "9.0um" "8.0um" "7.0um" "6.0um" "5.0um" "4.5um" "4.0um" "3.5um" "3.0um" "2.5um" "2.0um" "1.75um" "1.5um" "1.25um" "1.0um")
for bw in "${StringArray[@]}"; do
  (nohup ../bin/allpix -c Exp2.conf -o DepositionLaser.beam_waist=$bw > "output_exp2/EXPOUT_"$iarg) >& "output_exp2/EXPERR_"$iarg
  mv output/modulesExp.root "output_exp2/modulesExp_"$iarg".root"
  
  iarg=`expr $iarg + 1`
done


#iarg=13
#mkdir output_exp
#declare -a StringArray=("1.75um" "1.5um" "1.25" "1.0um")
#for bw in "${StringArray[@]}"; do
#  (../bin/allpix -c Exp.conf -o DepositionLaser.beam_waist=$bw > output_exp/EXPOUT_"$iarg") >& output_exp/EXPERR_"$iarg"
#  mv output/modulesExp.root output_exp/modulesExp_"$iarg".root
  
#  iarg=`expr $iarg + 1`
#done


#iarg=1
#mkdir output_test
#declare -a StringArray=("10.0um" "9.0um" "1.0um")
#for bw in "${StringArray[@]}"; do
#  (../bin/allpix -c Exp.conf -o DepositionLaser.beam_waist=$bw > output_test/TESTOUT_"$iarg") >& output_test/TESTERRtest_"$iarg"
#  mv output/modulesTest.root output_test/modulesTest_"$iarg".root
  
#  iarg=`expr $iarg + 1`
#done


