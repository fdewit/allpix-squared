iarg=1
#mkdir output_exp
declare -a StringArray=("10.0um" "9.0um" "8.0um" "7.0um" "6.0um" "5.0um" "4.5um" "4.0um" "3.5um" "3.0um" "2.5um" "2.0um" "1.75um" "1.5um" "1.25um" "1.0um")
for bw in "${StringArray[@]}"; do
  (../bin/allpix -c Exp5.conf -o DepositionLaser.beam_waist=$bw > "output_exp5/EXPOUT_"$iarg) >& "output_exp5/EXPERR_"$iarg
  mv output/modulesExp.root "output_exp5/modulesExp_"$iarg".root"
  
  iarg=`expr $iarg + 1`
done


iarg=1
#mkdir output_exp
declare -a StringArray=("10.0um" "9.0um" "8.0um" "7.0um" "6.0um" "5.0um" "4.5um" "4.0um" "3.5um" "3.0um" "2.5um" "2.0um" "1.75um" "1.5um" "1.25um" "1.0um")
for bw in "${StringArray[@]}"; do
  (../bin/allpix -c Exp6.conf -o DepositionLaser.beam_waist=$bw > "output_exp6/EXPOUT_"$iarg) >& "output_exp6/EXPERR_"$iarg
  mv output/modulesExp.root "output_exp6/modulesExp_"$iarg".root"
  
  iarg=`expr $iarg + 1`
done
