/**
 * @file
 * @brief Definition of gain reduction models
 *
 * @copyright Copyright (c) 2021-2023 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 * SPDX-License-Identifier: MIT
 */

#ifndef ALLPIX_GAINREDUCTION_MODELS_H
#define ALLPIX_GAINREDUCTION_MODELS_H

#include <limits>
#include <typeindex>

#include <numeric> //NEEDED???

#include <TFormula.h>
#include <Math/Point3D.h> //NEEDED???

#include "exceptions.h"

#include "core/config/Configuration.hpp"
#include "core/utils/log.h"
#include "core/utils/unit.h"
#include "objects/SensorCharge.hpp"
#include "objects/DepositedCharge.hpp" //NEEDED???
#include "ImpactIonization.hpp" //NEEDED????
//DETECTORMODEL NOT NEEDED???

namespace allpix {

    /**
     * @ingroup Models
     * @brief Gain reduction models
     */
    class GainReductionModel {
    public:
        /**
         * Default constructor
         * @param threshold Threshold below which the density is so low that we do not allow gain reduction
         */
        explicit GainReductionModel(double threshold) 
	    : threshold_(threshold), density_(0.), predicted_gain_(1.){};

        /**
         * Default virtual destructor
         */
        virtual ~GainReductionModel() = default;

        /**
         * Function call operator to obtain reduced gain value
         * @param gain Final gain of the propagated charge
         * @return Fraction of original gain after gain reduction calculation
         */
        virtual double operator()(double gain) const {
	    if(std::fabs(density_) < threshold_) {
	        LOG(DEBUG) << "Density is smaller than threshold. No gain reduction simulated.";
	        return 1.;}
            return reduction_factor(gain);
        };

        /**
         * Function call operator to obtain approximate density of the deposited charge
         * @param deposited_charge Deposited charge message for this event
         */
        virtual void setDensity(std::vector<DepositedCharge> deposited_charge) const {
	    density_ = calc_density(deposited_charge);
        };
	
        /**
         * Function call operator to obtain predicted value of the gain in the gain layer
         * @param config Configuration file
	 * @param detector Detector information to determine the electric field
         */
        virtual void predictGain(const Configuration& config, std::shared_ptr<const Detector> detector) const {
	    predicted_gain_ = predict_gain(config, detector);
        };

    protected:
   	double threshold_{std::numeric_limits<double>::max()};
	mutable double density_{0.};
	mutable double predicted_gain_{1.};
	
        virtual double reduction_factor(double gain) const = 0;
	virtual double calc_density(std::vector<DepositedCharge> deposited_charge) const = 0;
	virtual double predict_gain(const Configuration& config, std::shared_ptr<const Detector> detector) const = 0;
    };

    /**
     * @ingroup Models
     * @brief No gain reduction
     *
     */
    class NoGainReduction : virtual public GainReductionModel {
    public:
        NoGainReduction() : GainReductionModel(std::numeric_limits<double>::max()){};
        double operator()(double) const override { return 1.; };

    private:
        double reduction_factor(double) const override { return 1.; };
	double calc_density(std::vector<DepositedCharge>) const override {return 0.; };
	double predict_gain(const Configuration&, std::shared_ptr<const Detector>) const override {return 1.; };
    };

    /**
     * @ingroup Models
     * @brief Kramberger model for gain reduction
     */
    class Kramberger : virtual public GainReductionModel {
    public:
        Kramberger(double threshold, double thickness_gain_layer, std::string beam_shape)
            : GainReductionModel(threshold), 
	      b_(0.),
	      thickness_gain_layer_(thickness_gain_layer),
	      beam_shape_(beam_shape) {}

    private:
    
          /**
         * Function call operator to obtain reduced gain value
         * @param gain Final gain of the propagated charge
         * @return Fraction of original gain after gain reduction calculation
         */
        double reduction_factor(double gain) const override {
	    LOG(DEBUG) << "Reduction factor is " << std::pow(gain,-b_*density_);
            return std::pow(gain,-b_*density_);
        };
	
        /**
         * Function to calculate the approximate surface density of electron-hole pairs 
	 *     using a gaussian, minmax ellips or rectangular approximation of the area
         * @param deposited_charge deposits of charge carriers at every event
         * @return Approximate two dimensional density of charges in the gain layer
         */
	double calc_density(std::vector<DepositedCharge> deposited_charge) const override { 
	
	    // Initialize values
	    double total_charge = 0.;
	    double min_x = std::numeric_limits<double>::max();//deposited_charge[0].getLocalPosition().x();
	    double max_x = std::numeric_limits<double>::lowest();//deposited_charge[0].getLocalPosition().x();
	    double min_y = std::numeric_limits<double>::max();//deposited_charge[0].getLocalPosition().y();
	    double max_y = std::numeric_limits<double>::lowest();//deposited_charge[0].getLocalPosition().y();
	    
	    
	    std::vector<double> x_positions;
	    std::vector<double> y_positions;
	    std::vector<double> charges;
	    double FWHM_x = 0.;
	    double FWHM_y = 0.;
	    
	    auto FWHM = [&](std::vector<double> data, std::vector<double> weights) {
	        long unsigned int size = weights.size();
		LOG(DEBUG) << "Size of charges vector: " << size;
	        if(size <= 1) {return 0.;}
	        double sum = std::inner_product(data.begin(),data.end(),weights.begin(),0.0);
		double N = std::accumulate(weights.begin(),weights.end(),0.0);
		double mean = sum/N;
		double sq_sum = 0.;
		for (long unsigned int i =0 ;i < size; i++){
		    sq_sum += (data[i]-mean)*(data[i]-mean)*weights[i];
		}
		double std_dev = std::sqrt(sq_sum/N);
		return 2.355*std_dev;
	    };

	    
	    // Loop over deposits within the gain layer and keep track of total charge and minimum/maximum coordinates
	    for(const DepositedCharge& deposit : deposited_charge){
	        ROOT::Math::XYZPoint pos = deposit.getLocalPosition();
		if (pos.z() > max_z_ - thickness_gain_layer_){
	            total_charge += std::fabs(deposit.getCharge());
		    
		    if(beam_shape_ == "minmax" || beam_shape_ == "square") {	
		        if (pos.x() < min_x){ min_x = pos.x(); }
		        if (pos.x() > max_x){ max_x = pos.x(); }
		        if (pos.y() < min_y){ min_y = pos.y(); }
		        if (pos.y() > max_y){ max_y = pos.y(); }
		    }	
		    if(beam_shape_ == "gaussian") {
		        x_positions.push_back(pos.x());
		        y_positions.push_back(pos.y());
		        charges.push_back(std::fabs(deposit.getCharge()));
		    }
		    
		}
	    }
	    
	    if(beam_shape_ == "gaussian"){
	        FWHM_x = FWHM(x_positions, charges);
	        FWHM_y = FWHM(y_positions, charges);
	        LOG(DEBUG) << "FWHM of x " << Units::display(FWHM_x, "um") << " FWHM of y " << Units::display(FWHM_y, "um");
	        LOG(DEBUG) << "Total amount of charge in gain layer " << Units::display(total_charge, "e");
	    }
	    
	    // Approximate the area as a rectangle
	    //double area = (max_x-min_x)*(max_y-min_y);

	    double area = 0.;
	    if(total_charge==0.) {
	        // If no charge was deposited in the gain layer, set the area to 0 to avoid overflow
	        area = 0.;
	    }
	    else {
	        // Approximate the area as an oval
		if(beam_shape_ == "minmax")       { area = 3.14/4.*(max_x-min_x)*(max_y-min_y) ;}
		else if(beam_shape_ == "square")  { area = (max_x-min_x)*(max_y-min_y) ;}
		else if(beam_shape_ == "gaussian"){ area = 3.14/4.*FWHM_x*FWHM_y ;}
	    }
	    
	    if (area<Units::get(0.5,"um*um")){ 
	        //Very small areas may produce unphysically high densities
	        LOG(DEBUG) << "Area is " << Units::display(area,"um*um") << " which is too small, setting density to zero. No gain reduction simulated.";
	        return 0.;
	    }
	    
	    // Divide charge by 2 to get the amount of electron-hole pairs
	    total_charge /= 2.;
	     
	    // DEBUG
	    LOG(DEBUG) << "Area is " << Units::display(area,"um*um") << " and surface density is " << Units::display(total_charge/area,"/um/um");
	    
	    return total_charge/area;
	};
	
        /**
         * Function to predict the gain generated in the gain layer
         * @param config Configuration file
	 * @param detector Detector information used for the electric field
         * @return Prediction of the gain used to predict the total density in the gain layer
         */
	double predict_gain(const Configuration& config, std::shared_ptr<const Detector> detector) const override {
	
	    // Initialize a temporary multiplication model
	    ImpactIonization multiplication_temp = ImpactIonization(config);
	    double N = config.get<double>("gain_reduction_steps",100.);
	    
	    // Determine the location of the gain layer
	    double min_z = detector->getModel()->getSensorSize().z()/2. - thickness_gain_layer_;
	    max_z_ = detector->getModel()->getSensorSize().z()/2.;
	    
	    // Initialize values
	    double efield_mag = 0.;
	    double efield_average = 0.;
	    double average_gain = 1.;
	    
	    // Go through the gain layer in small steps
	    for (double i=0.; i<N; i++){
	        ROOT::Math::XYZPoint pos = ROOT::Math::XYZPoint(0.,0.,min_z+thickness_gain_layer_*i/N);
	        efield_mag = std::sqrt(detector->getElectricField(pos).Mag2());
		efield_average += efield_mag;
		average_gain *= multiplication_temp(CarrierType::ELECTRON, efield_mag, thickness_gain_layer_/N);
	    }
	    
	    // Divide by N to obtain the average
	    efield_average /= N;
	    	    
	    // DEBUG
	    LOG(DEBUG) << "Average predicted gain is " << average_gain << " and average field is " << Units::display(efield_average, "kV/cm");
	    
	    // Calculate the constant b from the paper by Kramberger E_c/E^2 * e_0/(epsilon*epsilon_0)*(G_average - 1)
	    // FIXME E_c is not 203 but 123??
	    // FIXED user can choose original (203) or adjusted (123)
	    auto E_c_model = config.get<bool>("gain_reduction_adjusted_bvalue", true);
	    double E_c = ( E_c_model ? Units::get(123.,"V/um") : Units::get(203.,"V/um"));
	    double e_0 = Units::get(1.,"e");
	    double epsilon_epsilon_0 = 11.68*Units::get(8.85e-12,"C/V/m");
	    b_ = E_c/(efield_average*efield_average)*e_0*(average_gain-1.)/(epsilon_epsilon_0);
	    LOG(DEBUG) <<"Parameter b is now " << Units::display(b_, "um*um");
	    return average_gain;
	}
	
        mutable double b_;
	double thickness_gain_layer_;
	std::string beam_shape_;
	mutable double max_z_;
    };

    /**
     * @brief Wrapper class and factory for gain reduction models.
     *
     * This class allows to store gain reduction objects independently of the model chosen and simplifies access to the
     * function call operator. The constructor acts as factory, generating model objects from the model name provided,
     * e.g. from a configuration file.
     */
    class GainReduction {
    public:
        /**
         * Default constructor
         */
        GainReduction() = default;

        /**
         * GainReduction constructor
         * @param config Configuration of the calling module
	 * @param detector Detector of the calling module
         */
        explicit GainReduction(const Configuration& config, std::shared_ptr<const Detector> detector) {
            try {
                auto model = config.get<std::string>("gain_reduction_model", "none");
                std::transform(model.begin(), model.end(), model.begin(), ::tolower);
                auto threshold = config.get<double>("gain_reduction_threshold", std::numeric_limits<double>::max());
		auto thickness_gain_layer = config.get<double>("thickness_gain_layer", 0.);
		auto beam_shape = config.get<std::string>("gain_reduction_beam_shape", "gaussian");

                if(model == "kramberger") {
                    model_ = std::make_unique<Kramberger>(threshold, thickness_gain_layer, beam_shape);
                } else if(model == "none") {
                    LOG(INFO) << "No gain reduction model chosen, gain reduction not simulated";
                    model_ = std::make_unique<NoGainReduction>();
                } else {
                    throw InvalidModelError(model);
                }
                LOG(INFO) << "Selected gain reduction model \"" << model << "\"";
            } catch(const ModelError& e) {
                throw InvalidValueError(config, "gain_reduction_model", e.what());
            }
	    
	    // After initialization, immediately predict the gain from the electric field
	    model_->predictGain(config, detector);
        }

        /**
         * Function call operator forwarded to the gain reduction model
         * @return Gain reduction
         */
        template <class... ARGS> double operator()(ARGS&&... args) const {
            return model_->operator()(std::forward<ARGS>(args)...);
        }

        /**
         * Function to get the density forwarded to the gain reduction model
         * @return Density
         */
        template <class... ARGS> void setDensity(ARGS&&... args) const {
            return model_->setDensity(std::forward<ARGS>(args)...);
        }
	
        /**
         * Function to predict the gain forwarded to the gain reduction model
         * @return Density
         */
        template <class... ARGS> void predictGain(ARGS&&... args) const {
            return model_->predictGain(std::forward<ARGS>(args)...);
        }

        /**
         * @brief Helper method to determine if this model is of a given type
         * The template parameter needs to be specified speicifcally, i.e.
         *     if(model->is<MyModel>()) { }
         * @return Boolean indication whether this model is of the given type or not
         */
        template <class T> bool is() const { return dynamic_cast<T*>(model_.get()) != nullptr; }

    private:
        std::unique_ptr<GainReductionModel> model_{};
    };

} // namespace allpix

#endif
